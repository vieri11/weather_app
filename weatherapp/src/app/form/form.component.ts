import { Component, OnInit } from '@angular/core';
import { WeatherDataService} from "../weather-data.service";
import {Weathecondtion} from "../Weathercondition";
import {map} from "rxjs/internal/operators";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass'],
})
export class FormComponent implements OnInit {

    condition:boolean = false;
    weather: Weathecondtion[];


    constructor(private _weatherDataService : WeatherDataService){
    }

  getCity(value:string){
    this._weatherDataService.cityWeather(value).subscribe(
        // res => console.log(res)
        res => this.weather = res

    );
      this.condition = true;
  }


  ngOnInit() {

  }


}
