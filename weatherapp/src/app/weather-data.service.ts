import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Weathecondtion } from "./Weathercondition";
import { environment } from '../environments/environment';
import { Observable } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class WeatherDataService {

    baseUrl:string = environment.baseUrl;
    key:string = environment.key;

  constructor(private http: HttpClient) {}

  cityWeather(value:string) : Observable<Weathecondtion[]>{
    let url:string = this.baseUrl+value+this.key;
    return this.http.get<Weathecondtion[]>(url)

  }




}
